# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/cherrypy/cherrypy-3.1.2.ebuild,v 1.7 2010/04/02 18:49:55 arfrever Exp $

EAPI="3"
SUPPORT_PYTHON_ABIS="1"
PYTHON_DEPEND="2:2.6 3:3.1"
RESTRICT_PYTHON_ABIS="2.[12345]"

inherit distutils

MY_PN="PyTTY"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="PyTTY is a pitiful serial access package for Python."
HOMEPAGE="http://pypi.python.org/pypi/PyTTY"
SRC_URI="mirror://pypi/${MY_PN:0:1}/${MY_PN}/${MY_P}.tar.gz"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

S="${WORKDIR}/${MY_P}"
