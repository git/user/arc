# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/cherrypy/cherrypy-3.1.2.ebuild,v 1.7 2010/04/02 18:49:55 arfrever Exp $

EAPI="3"
SUPPORT_PYTHON_ABIS="1"

inherit distutils eutils versionator

MY_PV=$(delete_version_separator _)
MY_P="CherryPy-${MY_PV}"

DESCRIPTION="CherryPy is a pythonic, object-oriented web development framework."
HOMEPAGE="http://www.cherrypy.org/ http://pypi.python.org/pypi/CherryPy"
SRC_URI="http://download.cherrypy.org/${PN}/${MY_PV}/${MY_P}.tar.gz
	http://download.cherrypy.org/${PN}/${MY_PV}/${MY_P}-py3.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND=""
RDEPEND=""

S="${WORKDIR}/${MY_P}"

src_unpack() {
	unpack ${MY_P}-py3.tar.gz
	mv "${S}" "${S}-py3"
	unpack ${MY_P}.tar.gz
}

src_prepare() {
	patching() {
		[[ "${PYTHON_ABI}" != 3.* ]] && return
		cd "${S}-py3"
		epatch "${FILESDIR}/${MY_P}-py3.patch"
	}
	python_execute_function --action-message 'Applying patches with $(python_get_implementation) $(python_get_version)' patching
}

distutils_src_compile_pre_hook() {
	if [[ "${PYTHON_ABI:0:1}" == "3" ]]; then
		cd "${S}-py3"
	else
		cd "${S}"
	fi
}

distutils_src_test_pre_hook() {
	if [[ "${PYTHON_ABI:0:1}" == "3" ]]; then
		cd "${S}-py3"
	else
		cd "${S}"
	fi
}

distutils_src_install_pre_hook() {
	if [[ "${PYTHON_ABI:0:1}" == "3" ]]; then
		cd "${S}-py3"
	else
		cd "${S}"
	fi
}

src_test() {
	testing() {
		if [[ "${PYTHON_ABI:0:1}" == "3" ]]; then
			cd "${S}-py3"
		else
			cd "${S}"
		fi
		PYTHONPATH="build-${PYTHON_ABI}/lib" "$(PYTHON)" cherrypy/test/test.py --dumb
	}
	python_execute_function testing
}

src_install() {
	distutils_src_install

	if use doc; then
		insinto "/usr/share/doc/${PF}"
		doins -r cherrypy/tutorial
	fi
}
