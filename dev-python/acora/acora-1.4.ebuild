# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
SUPPORT_PYTHON_ABIS="1"
PYTHON_DEPEND="*:2.5"

inherit distutils

DESCRIPTION="Fast multi-keyword search engine for text strings"
HOMEPAGE="http://pypi.python.org/pypi/acora"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=dev-python/cython-0.12.1"
RDEPEND="${DEPEND}"
